package com.devcamp.restapibookauthorv2.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.restapibookauthorv2.models.Book;

@Service
public class BookService {
  @Autowired
  private AuthorService authorService;

  ArrayList<Book> books = new ArrayList<>();

  public ArrayList<Book> getBooks() {
    return books;
  }

  public ArrayList<Book> getAllBooks() {
    ArrayList<Book> listBook = new ArrayList<>();

    Book book1 = new Book("Yoga", authorService.getAuthor1(), 100000, 1);
    Book book2 = new Book("Math", authorService.getAuthor2(), 120000, 2);
    Book book3 = new Book("Piano", authorService.getAuthor3(), 90000, 3);

    listBook.add(book1);
    listBook.add(book2);
    listBook.add(book3);

    books.addAll(listBook);
    return listBook;
  }

  public ArrayList<Book> getBookQuantity(int quantityNumber) {
    ArrayList<Book> listBook = new ArrayList<Book>();
    listBook.addAll(getAllBooks());
    ArrayList<Book> bookResult = new ArrayList<Book>();
    for (int counter = 0; counter < listBook.size(); counter++) {
      Book bookCheck = listBook.get(counter);
      if (bookCheck.getQty() >= quantityNumber) {
        bookResult.add(bookCheck);
      }
    }
    return bookResult;
  }

  public Book getBookByIndex(int index) {
    ArrayList<Book> listBook = new ArrayList<Book>();
    listBook.addAll(getAllBooks());
    Book bookResult = new Book();
    if (-1 < index && index < listBook.size()) {
      bookResult = listBook.get(index);
    }
    return bookResult;
  }
}
