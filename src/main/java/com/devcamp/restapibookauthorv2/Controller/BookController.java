package com.devcamp.restapibookauthorv2.Controller;

import java.lang.reflect.Array;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapibookauthorv2.models.Book;
import com.devcamp.restapibookauthorv2.service.BookService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class BookController {
  @Autowired
  private BookService bookService;

  @GetMapping("/books")
  public ArrayList<Book> getAllBook() {
    ArrayList<Book> books = bookService.getAllBooks();
    return books;
  }

  @GetMapping("/book-quantity")
  public ArrayList<Book> getBookQuantity(@RequestParam(name = "quantityNumber", required = true) int quantityNumber) {
    return bookService.getBookQuantity(quantityNumber);
  }

  @GetMapping("/books/{index}")
  public Book getBookByIndex(@PathVariable int index) {
    return bookService.getBookByIndex(index);
  }
}
