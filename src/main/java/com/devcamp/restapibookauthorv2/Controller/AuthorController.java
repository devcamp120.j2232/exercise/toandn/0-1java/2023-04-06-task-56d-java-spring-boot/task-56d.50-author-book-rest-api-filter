package com.devcamp.restapibookauthorv2.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapibookauthorv2.models.Author;
import com.devcamp.restapibookauthorv2.service.AuthorService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class AuthorController {
  @Autowired
  private AuthorService authorService;

  @GetMapping("/author-info/{email}")
  public Author getAuthorByEmail(@PathVariable String email) {
    return authorService.getAuthorByEmail(email);
  }

  @GetMapping("/author-gender/{gender}")
  public ArrayList<Author> getAuthorByGender(@PathVariable char gender) {
    return authorService.getAuthorByGender(gender);
  }
}
